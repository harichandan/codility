package com.harichandan.codility.Lesson2;

public class OddOccurrencesInArray {
	public int solution(int[] A) {
		int xor = 0;
		for (int elem : A)
			xor ^= elem;
		return xor;
	}
}

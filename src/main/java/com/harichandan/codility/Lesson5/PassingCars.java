package com.harichandan.codility.Lesson5;

public class PassingCars {
	public int solution(int[] A) {
		int zeroCount = 0, passes = 0;
		for (int i : A) {
			if (i == 0)
				zeroCount++;
			else
				passes += zeroCount;

			if (passes > 1000000000)
				return -1;
		}
		return passes;
	}
}
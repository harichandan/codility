package com.harichandan.codility.Lesson7;

import java.util.*;

class Brackets {
    public int solution(String S) {
        Deque<Character> stack = new LinkedList<>();
        for (char c : S.toCharArray()) {
            if (c == '(' || c == '{' || c == '[')
                stack.push(c);
            else {
                if (stack.isEmpty())
                    return 0;
                char d = stack.pop();
                if ((c == ')' && d != '(') || (c == '}' && d != '{') || (c == ']' && d != '['))
                    return 0;
            }
        }
        return stack.isEmpty() ? 1 : 0;
    }

    public int solution2(String s) {
        int bCount = 0, cCount = 0, sCount = 0;
        for (char c : s.toCharArray()) {
            switch (c) {
            case '(':
                bCount++;
                break;
            case '{':
                cCount++;
                break;
            case '[':
                sCount++;
                break;
            case ')':
                if (bCount <= 0)
                    return 0;
                else
                    bCount--;
                break;
            case '}':
                if (cCount <= 0)
                    return 0;
                else
                    cCount--;
                break;
            case ']':
                if (sCount <= 0)
                    return 0;
                else
                    sCount--;
                break;
            }
        }
        boolean balanced = (bCount == 0) && (cCount == 0) && (sCount == 0);
        return balanced ? 1 : 0;
    }
}
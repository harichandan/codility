package com.harichandan.codility.Lesson7;

class Nesting {
    public int solution(String S) {
        int count = 0;
        for (char c : S.toCharArray()) {
            if (c == '(')
                count++;
            else {
                if (count <= 0)
                    return 0;
                else
                    count--;
            }
        }
        return count == 0 ? 1 : 0;
    }
}

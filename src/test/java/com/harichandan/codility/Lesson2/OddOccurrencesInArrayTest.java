package com.harichandan.codility.Lesson2;

import org.junit.jupiter.api.Test;

class OddOccurrencesInArrayTest {
	@Test
	void test1() {
		assert (new OddOccurrencesInArray().solution(new int[] { 9, 9, 3, 3, 5 }) == 5);
	}

	@Test
	void test2() {
		assert (new OddOccurrencesInArray().solution(new int[] { 1 }) == 1);
	}
}

package com.harichandan.codility.Lesson5;

import org.junit.jupiter.api.Test;

class PassingCarsTest {
	PassingCars obj = new PassingCars();
	
	@Test
	void test1() {
		assert(obj.solution(new int[] {0,1,0,1,1}) == 5);
	}
	
	@Test
	void test2() {
		assert(obj.solution(new int[] {0,1}) == 1);
	}
	
	@Test
	void test3() {
		assert(obj.solution(new int[] {1,0}) == 0);
	}
	
	@Test
	void test4() {
		assert(obj.solution(new int[] {0}) == 0);
	}
	
	@Test
	void test5() {
		assert(obj.solution(new int[] {1}) == 0);
	}
}

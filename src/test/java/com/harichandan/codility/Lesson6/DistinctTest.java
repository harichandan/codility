package com.harichandan.codility.Lesson6;

import org.junit.jupiter.api.Test;

class DistinctTest {
	Distinct obj = new Distinct();

	@Test
	void test() {
		assert (obj.solution(new int[] { 2, 1, 1, 2, 3, 1 }) == 3);
	}

}

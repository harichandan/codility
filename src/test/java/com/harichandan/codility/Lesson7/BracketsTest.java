package com.harichandan.codility.Lesson7;

import org.junit.jupiter.api.Test;

class BracketsTest {
    Brackets obj = new Brackets();

    @Test
    void test() {
        assert (obj.solution("()") == 1);
    }

    @Test
    void test2() {
        assert (obj.solution("(()))") == 0);
    }

    @Test
    void test3() {
        assert (obj.solution("(((()))") == 0);
    }

    @Test
    void test4() {
        assert (obj.solution2("()") == 1);
    }

    @Test
    void test5() {
        assert (obj.solution2("(()))") == 0);
    }

    @Test
    void test6() {
        assert (obj.solution2("(((()))") == 0);
    }

}

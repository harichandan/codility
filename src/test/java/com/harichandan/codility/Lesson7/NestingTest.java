package com.harichandan.codility.Lesson7;

import org.junit.jupiter.api.Test;

class NestingTest {
    Nesting obj = new Nesting();

    @Test
    void test1() {
        assert (obj.solution("()") == 1);
    }

    @Test
    void test2() {
        assert (obj.solution(")") == 0);
    }

    @Test
    void test3() {
        assert (obj.solution("(") == 0);
    }

    @Test
    void test4() {
        assert (obj.solution("(()())") == 1);
    }

    @Test
    void test5() {
        assert (obj.solution("(()()(") == 0);
    }
}
